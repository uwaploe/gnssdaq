// Gnssdaq is the GNSS data acquisition program for the DOT Buoy.
package main

import (
	"context"
	"flag"
	"fmt"
	"log"
	"os"
	"os/signal"
	"runtime"
	"syscall"

	asterx "bitbucket.org/uwaploe/go-asterx"
	sbf "bitbucket.org/uwaploe/go-sbf"
	"github.com/gomodule/redigo/redis"
	"github.com/pkg/errors"
)

var Version = "dev"
var BuildDate = "unknown"

var Usage = `Usage: gnssdaq [options] cfgfile cmdfile

Collect SBF data blocks from a Septentrio GNSS receiver and publish them
to one or more Redis pub-sub channels. CFGFILE provides the mapping from
SBF block numbers to Redis channels.

CMDFILE provides the initial commands to send to the receiver.
`

var (
	showVers = flag.Bool("version", false,
		"Show program version information and exit")
	debug = flag.Bool("debug", false,
		"Output diagnostic information while running")
)

func loadFile(sensor *asterx.Device, cmds string) error {
	file, err := os.Open(cmds)
	if err != nil {
		return errors.Wrap(err, "open command file")
	}
	defer file.Close()
	return sensor.Upload(file)
}

func main() {
	flag.Usage = func() {
		fmt.Fprintf(os.Stderr, Usage)
		flag.PrintDefaults()
	}
	flag.Parse()

	if *showVers {
		fmt.Fprintf(os.Stderr, "%s %s\n", os.Args[0], Version)
		fmt.Fprintf(os.Stderr, "  Built with: %s\n", runtime.Version())
		os.Exit(0)
	}

	args := flag.Args()

	if len(args) < 2 {
		flag.Usage()
		os.Exit(1)
	}

	// Drop log timestamps when running under Systemd
	if _, ok := os.LookupEnv("JOURNAL_STREAM"); ok {
		log.SetFlags(0)
	}

	syscfg, err := loadConfig(args[0])
	if err != nil {
		log.Fatal(err)
	}

	p, err := syscfg.Ports["command"].openPort()
	if err != nil {
		log.Fatalf("Cannot open command port: %v", err)
	}

	// Open command-control device interface
	ccdev := asterx.New(p, "USB1>")
	if err := ccdev.GetPrompt(); err != nil {
		log.Fatalf("No response from GNSS: %v", err)
	}

	dport, err := syscfg.Ports["data"].openPort()
	if err != nil {
		log.Fatalf("Cannot open data port: %v", err)
	}

	conn, err := redis.Dial("tcp", syscfg.Redis.Addr)
	if err != nil {
		log.Fatal(err)
	}

	if err := loadFile(ccdev, args[1]); err != nil {
		log.Fatal(err)
	}

	// Construct the channel map
	cm := newChanMap(syscfg.Redis.Channels)

	// Initialize signal handler
	sigs := make(chan os.Signal, 1)
	signal.Notify(sigs, syscall.SIGINT, syscall.SIGTERM, syscall.SIGHUP)
	defer signal.Stop(sigs)

	ctx, cancel := context.WithCancel(context.Background())
	defer cancel()

	// Goroutine to wait for signals or for the signal channel
	// to close.
	go func() {
		s, more := <-sigs
		if more {
			log.Printf("Got signal: %v", s)
			cancel()
		}
	}()

	log.Printf("DOT Buoy GNSS data reader (%s)", Version)
	ch := sbf.StreamBlocks(ctx, dport)
	var chpub string
loop:
	for {
		select {
		case rec, more := <-ch:
			if !more {
				break loop
			}
			chpub = cm.lookup(int(rec.BlockNum()))
			if chpub != "" {
				conn.Do("PUBLISH", chpub, rec.Raw())
			}
			if *debug {
				log.Printf("ID=%d\n", rec.BlockNum())
			}
		case <-ctx.Done():
			break loop
		}
	}

}
