module bitbucket.org/uwaploe/gnssdaq

go 1.16

require (
	bitbucket.org/uwaploe/go-asterx v0.2.0
	bitbucket.org/uwaploe/go-sbf v0.4.0
	github.com/BurntSushi/toml v0.3.1
	github.com/gomodule/redigo v1.8.5
	github.com/pkg/errors v0.8.0
	github.com/tarm/serial v0.0.0-20180830185346-98f6abe2eb07
	golang.org/x/sys v0.0.0-20181107165924-66b7b1311ac8 // indirect
)
