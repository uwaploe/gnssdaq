package main

import (
	"io/ioutil"
	"time"

	"github.com/BurntSushi/toml"
	"github.com/pkg/errors"
	"github.com/tarm/serial"
)

type Duration struct {
	time.Duration
}

func (d *Duration) UnmarshalText(text []byte) error {
	var err error
	d.Duration, err = time.ParseDuration(string(text))
	return err
}

type sysConfig struct {
	Ports map[string]portConfig `toml:"ports"`
	Redis redisConfig           `toml:"redis"`
}

type portConfig struct {
	Device  string   `toml:"device"`
	Baud    int      `toml:"baud"`
	Timeout Duration `toml:"timeout"`
}

func (p portConfig) openPort() (*serial.Port, error) {
	if p.Device == "" {
		return nil, errors.New("No serial device specified")
	}

	cfg := &serial.Config{
		Name:        p.Device,
		Baud:        p.Baud,
		ReadTimeout: p.Timeout.Duration,
	}

	return serial.OpenPort(cfg)
}

type redisConfig struct {
	Addr     string      `toml:"address"`
	Channels []redisChan `toml:"chans"`
}

type redisChan struct {
	Name   string `toml:"name"`
	Blocks []int  `toml:"blocks"`
}

// Map SBF block ID numbers to Redis channels.
type channelMap struct {
	chans   map[int]string
	defChan string
}

func newChanMap(rcs []redisChan) channelMap {
	cm := channelMap{
		chans: make(map[int]string),
	}

	for _, rc := range rcs {
		if len(rc.Blocks) == 0 {
			cm.defChan = rc.Name
		} else {
			for _, b := range rc.Blocks {
				cm.chans[b] = rc.Name
			}
		}
	}

	return cm
}

// Return the Redis channel for an SBF block
func (cm channelMap) lookup(n int) string {
	name := cm.chans[n]
	if name == "" {
		name = cm.defChan
	}
	return name
}

func loadConfig(filename string) (cfg sysConfig, err error) {
	b, err := ioutil.ReadFile(filename)
	if err != nil {
		return cfg, errors.Wrap(err, "read config file")
	}
	err = toml.Unmarshal(b, &cfg)
	if err != nil {
		return cfg, errors.Wrap(err, "parse config file")
	}

	return cfg, err
}
